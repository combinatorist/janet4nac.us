---
title: "1. Bio - In fewer than 280 characters, introduce yourself to members of the Brigade Network who may not know you."
date: "2020-01-26"
---
Lifelong technophile & organizer w/eclectic experience: entrepreneur (SF Pet Hospital) emergency med/disaster researcher, house rehabber, Scout leader, patients’ rights activist & Open Data advocate. Brigade Cpt. & Midwest NAC rep. Work as RN in urban ER. Also, a Boomer - OK?😉
