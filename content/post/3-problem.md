---
title: "3. What do you see as a key problem that the NAC should help the Network solve in the next year?"
date: "2020-01-26"
---
COMMUNICATION! There’s a lot of it going on, but we constantly get feedback from Brigades and from each other that we’ve missed some important information. We’ve got a scattered array of platforms and wide range of personal communication preferences, some of which require dev-level technical understanding to use. As a “tech organization,” it’s natural to make assumptions about the ubiquity and ease of use of some platforms (e.g. GitHub) but that has inadvertently excluded some “otherly-savvy” members. This is far from a unique problem, yet we’ve not made enough headway in solving it. It might be time for outside professional help - maybe a Brigade + NAC + Network Team workshop session at Summit with a communications consultant? We can do better.
