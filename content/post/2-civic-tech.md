---
title: "2. The civic tech community is growing up, and I'll help it continue to mature by..."
date: "2020-01-26"
---
advocating for the immediate development of a comprehensive, collaborative, and transparent model of where Brigades fit in the overall CfA organization, including financially.

It’s conventional wisdom that businesses must eventually “mature” & shed many traits that drove their initial success, but CfA is not a conventional business. Its founding vision includes grassroots advocacy, exposure of structural injustices, & disruption of malign status quo systems (“Hack for Change!) That ethos spawned projects by scrappy Brigade volunteers, some which became viable, scalable products for CfA. Conventional businesses don’t include crucial interdependence on the presence and activities of a national network of highly skilled & mission-motivated VOLUNTEERS, whose continued participation depends 100% on affinity & mutually satisfying working relationships. Mgmt models, designed for employees, if applied to manage Brigades, are a poor fit. We need a biz model that encompasses our uniqueness!
